#define _POSIX_C_SOURCE 199309L

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <getopt.h>
#include <linux/input.h>

const struct input_event syn = {.type = EV_SYN, .code = SYN_REPORT, .value = 0};
const struct input_event caps_dn = {
    .type = EV_KEY, .code = KEY_CAPSLOCK, .value = 1};
const struct input_event caps_up = {
    .type = EV_KEY, .code = KEY_CAPSLOCK, .value = 0};

void print_usage(FILE *stream, const char *program) {
  // clang-format off
  fprintf(stream,
          "both-shift-capslock - send Caps Lock when Shift keys on both sides are down\n"
          "\n"
          "usage: %s [-h | [-t delay] [-m mode]]\n"
          "\n"
          "options:\n"
          "    -h          show this message and exit\n"
          "    -t delay    delay used for key sequences in microseconds (default: 20000)\n"
          "    -m mode     0: Caps Lock is tapped when a Shift key is released while another one is down\n"
          "                1: Caps Lock is tapped when a Shift key is pressed while another one is down\n"
          "                2: Caps Lock is pressed when a Shift key is pressed while another one is down\n"
          "                   and released when a Shift key is released while another one is down (default)\n",
          program);
  // clang-format on
}

int read_event(struct input_event *event) {
  return fread(event, sizeof(struct input_event), 1, stdin) == 1;
}

void write_event(const struct input_event *event) {
  if (fwrite(event, sizeof(struct input_event), 1, stdout) != 1)
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[]) {
  int mode = 1, delay = 20000;

  for (int opt; (opt = getopt(argc, argv, "ht:m:")) != -1;)
    switch (opt) {
    case 'h':
      return print_usage(stdout, argv[0]), 0;
    case 'm':
      switch (atoi(optarg)) {
      case 0:
        mode = 0;
        continue;
      case 1:
        mode = 1;
        continue;
      case 2:
        mode = 2;
        continue;
      default:
        continue;
      }
    case 't':
      delay = atoi(optarg);
      continue;
    }

  struct timespec delay_time = {.tv_sec = 0, .tv_nsec = 0};
  delay_time.tv_nsec = delay * 1e6;

  struct input_event input;

  enum { NONE = 0, LEFT = 1, RIGHT = 2, BOTH = 3 } state = NONE;

  setbuf(stdin, NULL), setbuf(stdout, NULL);

  while (read_event(&input)) {
    if (input.type == EV_MSC && input.code == MSC_SCAN)
      continue;

    if (input.type != EV_KEY || (input.value != 1 && input.value != 0) ||
        (input.code != KEY_LEFTSHIFT && input.code != KEY_RIGHTSHIFT)) {
      write_event(&input);
      continue;
    }

    // action
    if (state == BOTH && input.value == 0 && mode != 1) {
      if (mode == 0) {
        write_event(&caps_dn);
        write_event(&syn);
        nanosleep(&delay_time, &delay_time);
      }
      write_event(&caps_up);
      write_event(&syn);
      nanosleep(&delay_time, &delay_time);
    }

    write_event(&input);

    // state
    if (input.code == KEY_LEFTSHIFT)
      state = input.value ? state | LEFT : state & ~LEFT;
    if (input.code == KEY_RIGHTSHIFT)
      state = input.value ? state | RIGHT : state & ~RIGHT;

    // action
    if (state == BOTH && input.value == 1 && mode != 0) {
      write_event(&syn);
      nanosleep(&delay_time, &delay_time);
      write_event(&caps_dn);
      if (mode == 1) {
        write_event(&syn);
        nanosleep(&delay_time, &delay_time);
        write_event(&caps_up);
      }
    }
  }

  return EXIT_FAILURE;
}
