PREFIX ?= /usr/local
CFLAGS += -pedantic -Wall -Wextra -O3 -std=c99

all: build

build:
	$(CC) $(CFLAGS) both-shift-capslock.c -o both-shift-capslock

clean:
	rm -f both-shift-capslock

install:
	install -Dm755 both-shift-capslock -t "$(DESTDIR)$(PREFIX)/bin/"

uninstall:
	rm -f "$(DESTDIR)$(PREFIX)/bin/both-shift-capslock"
