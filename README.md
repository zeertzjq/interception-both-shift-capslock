# interception-both-shift-capslock

An [Interception Tools](https://gitlab.com/interception/linux/tools) plugin to send Caps Lock when Shift keys on both sides are down.

This plugin is useful for those who have Caps Lock mapped to some other keys and still want to have a way to trigger Caps Lock from the keyboard that is hard to trigger accidentally. Unlike the XKB option `shift:both_capslock` which makes Shift keys generate a `Caps_Lock` `KeyRelease` event when released, this plugin doesn't affect the function of single Shift key presses.

## Building and Installation

```sh
git clone https://gitlab.com/zeertzjq/interception-both-shift-capslock.git
cd interception-both-shift-capslock
make && sudo make install
```

Installation prefix defaults to `/usr/local`. You can change it by setting the `PREFIX` variable:

```sh
make && sudo make PREFIX=/opt install
```

## Usage

```
both-shift-capslock - send Caps Lock when Shift keys on both sides are down

usage: both-shift-capslock [-h | [-t delay] [-m mode]]

options:
    -h          show this message and exit
    -t delay    delay used for key sequences in microseconds (default: 20000)
    -m mode     0: Caps Lock is tapped when a Shift key is released while another one is down
                1: Caps Lock is tapped when a Shift key is pressed while another one is down (default)
                2: Caps Lock is pressed when a Shift key is pressed while another one is down
                   and released when a Shift key is released while another one is down
```

`both-shift-capslock` is an [Interception Tools](https://gitlab.com/interception/linux/tools) plugin. A suggested `udevmon` job configuration is:

```yaml
- JOB: "intercept -g $DEVNODE | both-shift-capslock | uinput -d $DEVNODE"
  DEVICE:
    LINK: "/path/to/keyboard/device"
```
